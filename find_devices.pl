#!/usr/bin/perl
#
# A script to help me find my devices using their WiFi MAC addresses,
# by listening for beacons from those devices.
#
# Cedric Nelson (cedric.nelson@gmail.com), Jan 2015

my @channels_2ghz = (1..11);
my @channels_5ghz = qw(36 40 44 48 52 56 60 64 100 104 108 112 116 120 124 128 132 136 140 149 153 157 161 165);
my $channels_count_2ghz = scalar(@channels_2ghz);

my @mac_list = qw(aa:bb:cc:dd:ee:ff a1:b2:c3:b4:a5:b6);
my $macs_string = join('|', @mac_list);
my $mac_regex = qr/$macs_string/i;

my $capture_command = "tcpdump -I -P -n -i en1";

sub set_channel {
    # Set channel
    my ($channel_2ghz, $channel_5ghz) = @_;

    my $channel_cmd = "/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport --channel="; # Just add channel~
    # Can also use --getinfo to get current channel, etc
    my $info_cmd = "/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport --getinfo";

    my $channel;
    if ($channel_2ghz and $channel_5ghz) {
        $channel = join(',', $channel_2ghz, $channel_5ghz);
    } else {
        $channel = $channel_2ghz;
    }

    my $current_time = time();
    print("$current_time\t");
    print("Setting channel to $channel\n");
    system($channel_cmd . $channel);
    my $output = `$info_cmd`;
    chomp($output);
    my ($op_mode) = ($output =~ /op mode: (\S+)/);
    my ($current_channel) = ($output =~ /channel: (\S+)/);
    if ($op_mode and $current_channel and $current_channel eq $channel) {
        print("Channel change confirmed: $current_channel ($op_mode mode)\n");
    } else {
        print("Channel change unconfirmed! $current_channel ($op_mode mode)\n");
    }
}

# This allows for us to change the channel
print("Disassociating from any active wifi connection\n");
my $disassociate_cmd = "/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport --disassociate";
system($disassociate_cmd);

# Open antenna capture
open(my $wifi_capture_pipe, '-|', $capture_command);

my $signal_lock = 0;
my $wait_interval = 10; # 10 seconds on each channel
my $chat_threshold = 3; # Seconds between chat notifications
my $start_time = time();
my $channel_index = 0;
set_channel($channels_2ghz[$channel_index]);
while (<$wifi_capture_pipe>) {
    my $line = $_;
    chomp($line);
    #print("LINE: $line\n");

    if ($line =~ $mac_regex) {
        print("Found you! $line\n");
        # Get the SNR
        my ($snr) = ($line =~ /(\-[\d\.]+dB) signal/);
        # Say that we've found the device (Karen or Vicki voice plz)
        my $current_time = time();
        if ($current_time > $last_say + $chat_threshold) {
            system("say -v \"Karen\" \"Device signal $snr\"");
            #system("say -v \"Vicki\" \"Device signal $snr\"");
            $last_say = $current_time;
        }
        $signal_lock = 1;
    }
} continue {
    my $current_time = time();
    if ($current_time > $start_time + $wait_interval && !$signal_lock) {
        # Time to hop channels, unless we have a signal lock
        if (($channel_index + 1) > $channels_count_2ghz) {
            print("Starting at first channel again\n");
            $channel_index = 0;
            set_channel($channels_2ghz[$channel_index]);
        } else {
            $channel_index++;
            set_channel($channels_2ghz[$channel_index]);
        }

        # Reset the timer after channel change
        $start_time = $current_time;
    }
}
